package ajin.recyclerview;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.widget.SimpleAdapter;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private RecyclerView recyclerView;
    private List<String> mDatas;
    private MyAdapter mAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initDatas();
        initView();
        mAdapter = new MyAdapter(this,mDatas);
        recyclerView.setAdapter(mAdapter);
        //SET AS LIN
//        LinearLayoutManager lin = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL,false);
//        recyclerView.setLayoutManager(lin);
        //GRIDVIEW
       // recyclerView.setLayoutManager(new GridLayoutManager(this,3));
        //
        recyclerView.setLayoutManager(new StaggeredGridLayoutManager(3,StaggeredGridLayoutManager.VERTICAL));


        //set div line
        recyclerView.addItemDecoration(new DividerItemDecoration(this,DividerItemDecoration.VERTICAL_LIST));

    }
    private void initView(){
        recyclerView = (RecyclerView)findViewById(R.id.recyclerView);

    }
    private void initDatas(){
        mDatas = new ArrayList<String>();
        for(int i = 'A';i<='Z';i++){
            mDatas.add(""+(char)i);

        }
    }
}
