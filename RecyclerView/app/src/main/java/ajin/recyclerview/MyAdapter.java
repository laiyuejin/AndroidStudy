package ajin.recyclerview;

import android.content.Context;
import android.support.v7.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ajin on 1/5/2016.
 */
class MyAdapter extends RecyclerView.Adapter<MyAdapter.MyViewHolder>{

    private LayoutInflater mLayoutInflater;
    private List<String> mDatas;
    private Context context;
    private List<Integer> mHeight;
    public MyAdapter(Context context,List<String> datas)
    {
        this.context = context;
        this.mDatas = datas;
        mLayoutInflater = LayoutInflater.from(context);
        mHeight = new ArrayList<Integer>();
        for(int i=0;i<mDatas.size();i++){
            mHeight.add((int)(100+Math.random()*300));
        }
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mLayoutInflater.inflate(R.layout.layout_item,parent,false);
        MyViewHolder viewHolder = new MyViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        ViewGroup.LayoutParams params = holder.itemView.getLayoutParams();
        params.height = mHeight.get(position);
        holder.itemView.setLayoutParams(params);
        holder.v.setText(mDatas.get(position));
    }

    public void addData(int pos){
        mDatas.add("new one"+pos);
        notifyItemInserted(pos);
    }

    public void remove(int pos){
        mDatas.remove(pos);
        notifyItemRemoved(pos);
    }
    @Override
    public int getItemCount() {
        return mDatas.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder{
        TextView v;
        public MyViewHolder(View itemView) {
            super(itemView);
            v = (TextView)itemView.findViewById(R.id.textView);
        }
    }
}
